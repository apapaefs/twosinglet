# TwoSinglet

Code for the Two Real Singlet Extension of the SM (TRSM), based on the work that appears arXiv:2101.00037 by Andreas Papaefstathiou, Tania Robens, and Gilberto Tetlalmatzi-Xolocotzi. 

- MG5_aMC model: put into models subdirectory. There are three scalars: h, eta0 and iota0. Note that this is a python 2 model, so conversion will be necessary (should work automatically in MG5_aMC).
- The input parameters for this model are: a12 (theta_hs), a13 (theta_hx), a23 (theta_sx) and the scalar couplings, called kapijk(l) in this model. 
- twosinglet_scalarcouplings/twosinglet_generatecard.py will generate the param_card.dat input for MG5_aMC. See code for an example set of parameters. 
- Please cite:

```
@article{Papaefstathiou:2020lyp,
    author = "Papaefstathiou, Andreas and Robens, Tania and Tetlalmatzi-Xolocotzi, Gilberto",
    title = "{Triple Higgs Boson Production at the Large Hadron Collider with Two Real Singlet Scalars}",
    eprint = "2101.00037",
    archivePrefix = "arXiv",
    primaryClass = "hep-ph",
    reportNumber = "SI-HEP-2020-34, RBI-ThPhys-2020-53",
    month = "12",
    year = "2020"
}
```
